//
//  ContentView.swift
//  CI-Demo-iOS
//
//  Created by Gabriel Lourenco on 18/05/20.
//  Copyright © 2020 DevOdds. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World! DevOdds is here for IOS")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
